console.log("Hello, world!");
console. log("Hello, Batch 203")
console.
log
(
	"Hello, everyone!"
)


// Comments: 

// This is a single line comment.
/*
	This is a 
	Multiline comment
	ctrl + shft + /
*/

// Syntax and Statements

// Statements in programming are instructions that we tell the computer to perform

// Syntax in programminng, it is the set of rules that describes how statements must be constructed.

// Variables
/*
	- it is used to contain data

	- Syntax in declaring variables
		-let/const variableNameSample;

		-const-di nagbabago
		-let - nagbabago yung value nung variable
*/

let myVariable = "Hello";

console.log(myVariable)

/*console.log(hello);

let hello;*/

/*
	
*/
/* Declaring a variable with initial value
	let/const variableName = value;
*/

let productName = 'desktop computer'
console.log(productName);

let product = "Alvin's computer"
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// let- we usually change/reassign the values in our values

// Reassigning variable values
// Syntax
	// variableName = newValue;

productName = "Laptop";
console.log(productName)

let friend = "Kate";
friend = "Jane";

console.log(friend);// error because: Identifier "friend" has already been declared.

// interest = 4.489;
console.log(interest); 

// Declare ng valiable
let supplier;
// initialize
supplier = "John Smith Tradings";
console.log(supplier);

// reassign
supplier = "Zuitt Store";
console.log(supplier);

const pi =3.1416;
console.log(pi);

// var(ES1) vs let/cons (ES6)

/*a = 5;
console.log(a);
var a;*/

//Multiple variable declaraions

let	productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand)

/*// Using a variable with a reserved keyword
const let = "hello";
console.log(let); // error: cannot used reserved keyword as a variable name.*/

// [Section] Data Types
// Strings - series of characters that create a word, a phrase, a sentence or anything related in creating text.

let country = 'Philippines';
let province = 'Metro Manila';


// concatenation strings
let fullAddress = province + ' , ' + country;
console.log(fullAddress);

let greeting = "I live in the" + country;
console.log(greeting);

// console.log(country + province);

// Escape characters(\)
// \n refers to creating new line or set the text to next line
// https://www.tutorialspoint.com/escape-characters-in-javascript escape characters
let mailAddress = "Metro Manila\nPhilippines"
console.log(mailAddress);

let message = "John's employees went home early.";
console.log(message);	
message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole numbers
let headcount = 26;
console.log(headcount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

//Combine number and strings
console.log("John's grade last quarter is " + grade);

// Boolean
// true / false 

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);


// Arrays - it is used to store multiple values with the same data type.
// let/const arrayName = [elementA, elementB, elementC, . .]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

	// different data types
	// Storing different data types inside an array is not recommended because it will not make sense to in the context of programming.
	let details = ["John", "Smith", 32, true]
	console.log(details);

// Objects - it is a special kind of data type that's used to mimic real world objects/items.
// Syntax
	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value
		}
	*/

	let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		gender: 'Male',
		isMarried: false,
		contact: ["+63917 123 4567", "8123 4567"],
		address:{
			houseNumber: "345",
			city: "Manila"
		}
	};

	console.log(person);

	// Create abstract object
	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thirdGrading: 90.2,
		fourthGrading: 94.6
	}
	console.log(myGrades);

// to determine the data type of the variable or data value
	console.log(typeof myGrades);

	console.log(typeof grades);


	// Constant Objects and Arrays
	// we cannot reassign the value of the variable but we can change the elements of the constant array
	const anime = ["one piece", "One punch man", "attack on titans"]

	anime[0] = "kimetsu no yaiba";

	console.log(anime);


	// Null it is used to intentionally express the absence of the value in a variable declaration/initialization.

	let spouse = null;
	console.log(spouse);

	let myNumber = 0;
	let myString = "";

	console.log(myNumber);
	console.log(myString);


	//undefined - represents the state of a variable that has been declared but without an assigned value.
	let fullName;
	console.log(fullName);

	


